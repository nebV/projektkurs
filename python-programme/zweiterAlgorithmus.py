#!/usr/bin/env python3

#region Imports
from ev3dev2.button import Button
from ev3dev2.sensor.lego import TouchSensor, ColorSensor, GyroSensor
from ev3dev2.motor import LargeMotor, MoveTank, OUTPUT_B, OUTPUT_C
#endregion

#region Schnittstellen zum Roboter
tank = MoveTank(OUTPUT_B, OUTPUT_C)
ts = TouchSensor()
cl = ColorSensor('in4')
cr = ColorSensor('in2')
gy = GyroSensor()
#endregion

#region Variablen
MOVERIGHTDONE = -42
LEFT = 1
RIGHT = -1
turningSpeed = 20 # Drehgeschwindigkeit der Motoren
posx = 0 # momentane x-Position
posy = 0 # momentane y-Position
d = 1 # Richtung: 0 top, 1 right, 2 down, 3 left
paths = {} # alle Regionen, die der Roboter abfährt (keine Liste, da die keine negativen Indizes haben können)
moveLength = 1.625 # Anzahl der Umdrehungen, die der Roboter pro Bewegung zurücklegt (eine Roboterlänge inklusive Rahmen: 168 mm)
threshold = 5 # Sensitivität der Farbsensoren
#endregion

#region Hilfsfunktionen

#region Drehung um 90° nach rechts
def turnRight():
	global d
	tank.on(-turningSpeed/10, -turningSpeed/10)
	tank.on(turningSpeed, -turningSpeed)
	gy.wait_until_angle_changed_by(90)
	tank.off()
	print('turned right 90 degrees')
	d += 1
	d %= 4
#endregion

#region Drehung um 90° nach links
def turnLeft():
	global d
	tank.on(-turningSpeed/10, -turningSpeed/10)
	tank.on(-turningSpeed, turningSpeed)
	gy.wait_until_angle_changed_by(90)
	tank.off()
	print('turned left 90 degrees')
	d -= 1
	if d == 0:
		d = 3
#endregion

#region Drehung um beliebige Gradzahl in eine beliebige Richtung
def turnDeg(deg, lr): # lr: 1 = links; -1 = rechts
	tank.on(-turningSpeed * lr, turningSpeed * lr)
	gy.wait_until_angle_changed_by(deg)
	tank.off()
	print('turned by ' + str(deg) + ' degrees')
#endregion

#region Drehung um 180° (nur kosmetische Gründe)
def turnAround():
	global d
	turnDeg(180)
	d += 2
	d %= 4
#endregion

#region gibt zurück ob sich links vom Roboter ein Objekt befindet
def checkLeft():
	return cl.raw < (threshold, threshold, threshold)
#endregion

#region gibt zurück ob sich rechts vom Roboter ein Objekt befindet
def checkRight():
	return cr.raw < (threshold, threshold, threshold)
#endregion

#region gibt die neue Position nach einer Bewegung zurück
def newLocation(d, posx, posy): 
	if d == 0: 
		return posx, posy - 1
	elif d == 1:
		return posx + 1, posy
	elif d == 2:
		return posx, posy + 1
	elif d == 3:
		return posx - 1, posy
#endregion

#region gibt die Position links oder rechts vom Roboter zurück
def dirLocation(d, posx, posy, checkD): # checkD = 1: left, -1: right
	if d == 0:
		return posx - checkD, posy
	elif d == 1:
		return posx, posy - checkD
	elif d == 2:
		return posx + checkD, posy
	elif d == 3:
		return posx, posy + checkD
#endregion

#region bewegt den Roboter vorwärts
def moveForward(d, posx, posy):
	tank.on_for_rotations(turningSpeed, turningSpeed, moveLength)
	return newLocation(d, posx, posy)
#endregion

#region fügt mögliche offene Wege links und rechts zur Liste der offenen Wege hinzu
def updatePaths(d, posx, posy):
	#links:
	if not paths.get(posy - LEFT):
		paths[posy - LEFT] = []

	if checkLeft():
		newX, newY = dirLocation(d, posx, posy, LEFT)
		newPathFound = True
		for i in paths[newY]:
			if newX - i[0] == 1:
				i[0] = newX
				newPathFound = False
				break
			elif i[1] - newX == 1:
				i[1] = newX
				newPathFound = False
				break
		if newPathFound:
			paths[newY].append([posx, posx, False])

	#rechts:
	if not paths.get(posy - RIGHT):
		paths[posy - RIGHT] = []

	if checkRight():
		newX, newY = dirLocation(d, posx, posy, RIGHT)
		newPathFound = True
		for i in paths[newY]:
			if newX - i[0] == 1:
				i[0] = newX
				newPathFound = False
			elif i[1] - newX == 1:
				i[1] = newX
				newPathFound = False
		if newPathFound:
			paths[newY].append[posx, posx, False]
#endregion

#region gibt zurück ob der Roboter gegen eine Wand gefahren ist
def checkWall():
	return ts.value
#endregion

#region gibt den x-Wert des offenen Weges in der nächsten Spalte (rechts) zurück, der am nächsten zum aktuellen x-Wert des Roboters ist, sofern es eine Spalte weiter rechts gibt
def findNextX(d, posx, posy, lr):
	newX, newY = dirLocation(d, posx, posy, lr)
	if not len(paths[newY]) == 0:
		minDist = 10000
		x = -1
		v = -1
		for i in paths[newY]:
			if not i[2]:
				if newX > i[0] and newX < i[1]:
					return newX
				if minDist > abs(newX - i[0]):
					minDist = abs(newX - i[0])
					x = i[0]
					v = i
				if minDist > abs(newX - i[1]):
					minDist = abs(newX - i[1])
					x = i[1]
					v = i
		if v != -1:
			v[2] = True
		return x
	else:
		return MOVERIGHTDONE
#endregion

#region bewegt den Roboter zu einer bestimmten x-Position in der linken oder rechten Spalte
def moveToX(d, posx, posy, lr, moveX):
	while(posx < moveX):
		moveForward()
	if lr == RIGHT:
		turnRight()
		moveForward()
		turnLeft()
	else:
		turnLeft()
		moveForward()
		turnRight()
#endregion

def countOpenPaths():
	global paths
	count = 0
	for i in paths:
		for j in i:
			if j[2]:
				count += 1
	return count

def checkOpen(d, posx, posy, lr):
	global paths
	y = posy - lr
	for i in paths[y]:
		if not i[2]:
			i[2] = True
			if abs(posx - i[0]) < abs(posx - i[1]):
				return i[0]
			else:
				return i[1]
	return -1

#endregion

#region Hauptloop
def mainLoop(d, posx, posy):
	global paths
	updatePaths(d, posx, posy)
	openPaths = countOpenPaths()
	while openPaths > 0:
		for i in range(2): # fährt einmal vor und wieder zurück, jeweils bis er ein Hindernis erreicht, falls er nicht am niedrigsten Punkt gestartet ist
			while not checkWall():
				moveForward(d, posx, posy)
				updatePaths(d, posx, posy)
			turnAround()
		# go to next row
		moveX = findNextX(d, posx, posy, RIGHT)
		if moveX == -1:
			print('invalid thing')
			break
		elif moveX == MOVERIGHTDONE:
			print('rightmost point reached, backtracking')
			while openPaths > 0:
				missedX = checkOpen(d, posx, posy, LEFT)
				while missedX != -1:
					moveToX(d, posx, posy, LEFT, missedX)
					mainLoop(d, posx, posy)
					missedX = checkOpen(d, posx, posy, LEFT)
				missedX = checkOpen(d, posx, posy, RIGHT)
				while missedX != -1:
					moveToX(d, posx, posy, RIGHT, moveX)
					mainLoop(d, posx, posy)
					missedX = checkOpen(d, posx, posy, LEFT)
				turnLeft()
				moveForward()
				turnRight()
				openPaths = countOpenPaths()
		else:
			moveToX(d, posx, posy, RIGHT, moveX)
		openPaths = countOpenPaths()
#endregion

mainLoop(d, posx, posy)
