visited = []
field = [[]]
s = 50
w = -1
h = -1
prob = .3
startx = 0
starty = 0

def setup():
    global w, h, s, field
    size(800, 800)
    w = (width) / s
    h = height / s
    field = [[floor(random(1.5)) for i in range(s)] for j in range(s)]
    findStart()
    print(startx, starty)
    floodFill(startx, starty)

def findStart():
    global field, s, startx, starty
    for i in range(s):
        for j in range(s):
            if field[j][i] != 1:
                startx = j
                starty = i
                return

def floodFill(x, y):
    global visited
    if [x, y] in visited:
        return
    else:
        visited.append([x, y])
        if not checkWall(x, y - 1):
            floodFill(x, y - 1)
        if not checkWall(x - 1, y):
            floodFill(x - 1, y)
        if not checkWall(x, y + 1):
            floodFill(x, y + 1)
        if not checkWall(x + 1, y):
            floodFill(x + 1, y)
        return

def checkWall(tx, ty):
    global field, visited
    if tx < 0 or ty < 0 or tx >= s or ty >= s or [tx, ty] in visited:
        return True
    return field[tx][ty] == 1

def draw():
    global w, h, s, field
    for i in range(s):
        for j in range(s):
            c = color(255)
            if field[j][i] == 1:
                c = color(0)
            if [j, i] in visited:
                c = color(map(visited.index([j, i]), 0, len(visited), 0, 255), 0, 255)
            fill(c)
            rect(j * w, i * h, w, h)
