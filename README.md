# Erkunden eines Raums mit dem EV3

Hier finden Sie die Programme, die im Laufe dieses Projekts entstanden sind.

Zum Ausführen der Python-Programme müssen diese auf einen EV3 mit dem Betriebssystem [ev3dev](https://ev3dev.org) kopiert werden. Dann sollten sie im Datei-Browser des Roboters angezeigt werden und können durch Drücken des mittleren Knopfs ausgeführt werden.

Für die Visualiserungen wird die Programmierumgebung [Processing](https://processing.org) benötigt.