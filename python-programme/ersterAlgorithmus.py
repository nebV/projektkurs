#!/usr/bin/env python3

import os, atexit
from ev3dev2.button import Button
from ev3dev2.sensor.lego import TouchSensor, ColorSensor, GyroSensor
from ev3dev2.motor import LargeMotor, MoveTank, OUTPUT_B, OUTPUT_C

true = True
false = False
x = true
turningPower = 20
turningDegree = 1.625 # (eine Roboterlänge inklusive Rahmen: 168 mm)
os.system('echo "" > text.txt')

#region turning
def turnRight():
    tank.on(turningPower, -turningPower)
    gy.wait_until_angle_changed_by(90)
    tank.off()


def turnLeft():
    tank.on(-turningPower, turningPower)
    gy.wait_until_angle_changed_by(90)
    tank.off()

def turnDeg(deg):
    tank.on(-turningPower, turningPower)
    gy.wait_until_angle_changed_by(deg)
    tank.off()
#endregion

def changePos():
    global posy
    global posx
    global d

    if d == 0:
        posy -= 1
    elif d == 1:
        posx += 1
    elif d == 2:
        posy += 1
    elif d == 3:
        posx -= 1

def moveToPoint():
    newposes = []
    global paths
    global d
    d = d
    global visited
    s = paths.pop()
    print(s)
    tank.on_for_rotations(-50, -50, .5)
  
    ind = visited.index([s[0],s[1]])
    for i in range(len(visited) - 1, ind, -1):
        cur = visited[i]
        os.system('echo "MTP: ' + str(ind) + ': ' + str(cur) + '" >> text.txt')
        newposes.insert(0, cur)
        dd = -1
        if cur[0] < posx:
            dd = 1
        elif cur[0] > posx:
            dd = 3
        elif cur[1] < posy:
            dd = 2
        elif cur[1] > posy:
            dd = 0
        while d < dd:
            d += 1
            turnRight()
        while d > dd:
            d -= 1
            turnLeft()
        changePos()
        tank.on_for_rotations(turningPower, turningPower, turningDegree)
    dd = s[2]

    while d < dd:
        d += 1
        turnRight()
    while d > dd:
        d -= 1
        turnLeft()
    for i in newposes:
        visited.append(i)


d = 1
paths = []
visited = []
posx = 0
posy = 0

tank = MoveTank(OUTPUT_B, OUTPUT_C)
btn = Button()
ts = TouchSensor()
cl = ColorSensor('in4')
cr = ColorSensor('in2')
gy = GyroSensor()


while len(paths) > 0 or x:
    visited.append([posx, posy])
    if cl.raw < (5, 5, 5):
        tmp = d - 1
        if tmp < 0:
            tmp = 3
        paths.append([posx, posy, tmp])
    if cr.raw < (5, 5, 5):
        tmp = d + 1
        if tmp > 3:
            tmp = 0
        paths.append([posx, posy, tmp])
    if len(paths) > 0 or ts.value():
        x = false
    newposx = posx
    newposy = posy
    if d == 0:
        newposy -= 1
    elif d == 1:
        newposx += 1
    elif d == 2:
        newposy += 1
    elif d == 3:
        newposx -= 1

    if ts.value() or [newposx, newposy] in visited:
        os.system('echo "in visited: ' + str([newposx, newposy] in visited) + '" >> text.txt')
        os.system('echo "paths: ' + str(paths) + '" >> text.txt')
        os.system('echo "visited: ' + str(visited) + '" >> text.txt')
        os.system('echo "pos: ' + str([posx, posy]) + '" >> text.txt')
        os.system('echo "d: ' + str(d) + '" >> text.txt')
        moveToPoint()
        print('Object found')
    tank.on_for_rotations(turningPower, turningPower, turningDegree)
    changePos()

tank.off()
