String[] path;
int maxX = 0;
int maxY = 0;
float sizeX;
float sizeY;

// Liest die Datei text.txt im gleichen Ordner aus, nimmt aus jeder Zeile die x- und y-Koordinaten und füllt die jeweiligen Koordinaten weiß, alle Objekte schwarz.

void setup() 
{
  path = loadStrings("./text.txt");
  size(1200, 900);

  for (String way : path)
  {
    if (int(splitTokens(way, "|")[0]) > maxX)
    {
      maxX = int(splitTokens(way, "|")[0]);
    }
    if (int(splitTokens(way, "|")[1]) > maxY)
    {
      maxY = int(splitTokens(way, "|")[1]);
    }
  }
  maxX++;
  maxY++;
  println(maxX, maxY);
  sizeX = (width - 200 ) / float(maxX);
  sizeY = (height - 200) / float(maxY);
  println(sizeX, sizeY);
  noStroke();
}

void draw()
{
  background(0);
  fill(255);
  for (String way : path)
  {
    rect(100 + float(splitTokens(way, "|")[0]) * sizeX, 100 + float(splitTokens(way, "|")[1]) * sizeY, sizeX, sizeY);
  }
}
