#!/usr/bin/env python3

from ev3dev2.button import Button
from ev3dev2.sensor.lego import TouchSensor, ColorSensor, GyroSensor
from ev3dev2.motor import LargeMotor, MoveTank, OUTPUT_B, OUTPUT_C
from os import *
from ev3dev2.display import Display
import ev3dev2.fonts as fonts

# Flood Fill Algorithmus

# region Initialisierung
# region Roboter Schnittstellen
tank = MoveTank(OUTPUT_B, OUTPUT_C)
cl = ColorSensor('in4')
ts = TouchSensor()
gy = GyroSensor()
display = Display()
# endregion
# region Konstanten
turningSpeed = 20  # Drehgeschwindigkeit der Motoren
moveLength = 1.625  # Anzahl der Umdrehungen, die der Roboter pro Bewegung zurücklegt (eine Roboterlänge inklusive Rahmen: 168 mm)
threshold = .5
turnFactor = 0.02
# endregion
visited = [] # Liste aller besuchten Punkte
direction = 0
dirs = [[0, -1], [-1, 0], [0, 1], [1, 0]]
#endregion

# region Hilfsfunktionen

# gibt zurück ob sich vor dem Roboter eine Wand befindet
def checkWall():
    tank.on_for_rotations(turningSpeed, turningSpeed, .5)
    val =  ts.value()
    tank.on_for_rotations(-turningSpeed, -turningSpeed, .5)
    return val
    #return cl.raw > (threshold, threshold, threshold)

# dreht den Roboter um 90° nach links
def turnLeft():
    turnDeg(90)
    #print('turned left 90 degrees')

# dreht den Roboter um deg Grad nach links (Drehungen nach rechts sind für dieses Programm nicht nötig)
def turnDeg(deg):
    tank.on(-turningSpeed, turningSpeed)
    gy.wait_until_angle_changed_by(deg - (turnFactor * deg))
    tank.off()
    #print('turned by ' + str(deg) + ' degrees')

# dreht den Roboter um 180°
def turnAround():
    turnDeg(180)

# bewegt den Roboter um eine moveLength nach vorne
def moveForward():
    tank.on_for_rotations(turningSpeed, turningSpeed, moveLength)

# bringt den Roboter zurück zu dem Platz an dem er vor dem Flood-Fill Aufruf war 
def movement():
    tank.on_for_rotations(-turningSpeed, -turningSpeed, moveLength)

# endregion

# Hauptalgorithmus: Flood Fill (https://de.wikipedia.org/wiki/Floodfill) für vier Nachbarn (oben, unten, links, rechts), rekursive Funktion die in von einem Punkt
# aus in jede Richtung nach Wänden (original: anderen Farben) testet und sich selbst mit der neuen Position aufruft falls in der Richtung keine Wand ist und der
# Roboter dort noch nicht gewesen ist
def floodFill(x, y):
    global visited
    global direction
    #display.clear()
    #display.draw.text((89, 64), str(x) + ':' + str(y), font=fonts.load('luBS24'))
    #display.update()
    if [x, y] in visited:
        return
    else:
        visited.append([x, y])
        for i in range(4):
            a = [x + dirs[direction][0], y + dirs[direction][1]]
            #display.clear()
            #display.draw.text((69, 64), str(a[0]) + ':' + str(a[1]) + '\n' + str(visited), font=fonts.load('luBS24'))
            #display.update()
            if not (a) in visited:
                if not checkWall():
                    moveForward()
                    floodFill(a[0], a[1])
                    movement()
            turnLeft()
            direction = (direction + 1) % 4
        return

# Aufruf des Algorithmus mit Startkoordinaten 0, 0
floodFill(0, 0)

# Ausgabe der besuchten Punkte auf das Display und in eine Datei
for i in visited:
    system('echo {0}:{1} >> text.txt'.format(i[0], i[1]))
    #print('{0}|{1}'.format(i[0], i[1]))
